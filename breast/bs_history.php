<?php

/**

 * 淋巴瘤数据库数据迁移

 * create_time:2016-05-03

 * encoding : UTF-8

 */

set_time_limit(0);

ini_set("display_errors","ON");

error_reporting(E_ALL);
//error_reporting(0);
define("BRD_WEB_ROOT", "/usr/local/var/www/test/");
require BRD_WEB_ROOT . "brdweb/base/config/base.inc.php";

date_default_timezone_set('Asia/Shanghai');

//offline  测试库  line 线上库
define("DB_CONFIG", 'offline');

define('define_limit',1000);
//本地数据库库名
define('DB','data-tranf');
//旧库的数据结构
define('OLD_DB_TABLE','bs_history');
//新库数据
define('NEW_DB_TABLE','bs_history_data');
//操作基本数据
define('BASE_DB_TABLE','emr_patients');


//结果输出文件
define('RESULT_PATH',"/tmp/transfer/lym.txt");
if (!file_exists(dirname(RESULT_PATH))) {
    mkdir(dirname(RESULT_PATH),0777,true);
}
file_put_contents(RESULT_PATH,'');
define('RESULT_PATH1',"/tmp/transfer/ly.txt");
if (!file_exists(dirname(RESULT_PATH1))) {
    mkdir(dirname(RESULT_PATH1),0777,true);
}
file_put_contents(RESULT_PATH1,'');


require '../script_pdo_config.php';

echo "\n**************************start***************\n";

//脚本运行开始

$start =  time();

echo "\nSCRIT RUN AT: ", date('Y-m-d H:i:s', time()), "\n";
//1.把旧库的数据转化成新库的数据
//2.处理数据（数据合并，加密字段解密）
main();


echo "\nSCRIT END AT: ", date('Y-m-d H:i:s', time()), "\n";

echo "TOTAL TIME : " . (time() - $start);

echo "\n++++++++++++++++++++++++++++++++++++++++++++OK++++++++++++++++++++++++++++++++++++++++++++++++++\n";


die;


function main(){
    $pdo_config         = new MyPDOconfig(DB,DB_CONFIG);
	
	echo "1: 获取新的表列名======开始\n";
    $GLOBALS['db_obj']  =  $pdo_config->init_db();
	//根据新字段创建新表
	$sql = "select new_en_filed_name from ". OLD_DB_TABLE ." group by new_en_filed_name";
	$data_list = $GLOBALS['db_obj']->query($sql);
	$createsql = "create table if not exists ". NEW_DB_TABLE ." ( id int(11) auto_increment, data_from varchar(30), hospital varchar(30), h_subject varchar(30), import_id varchar(20)," ;
			
	for ($i = 0; $i < count($data_list); $i++) {
		if(!empty($data_list[$i]['new_en_filed_name'])){
			$createsql .= $data_list[$i]['new_en_filed_name']." varchar(800) ,";
		}
	}
	$createsql .= "primary key (id) ) default character set utf8";
	
	$result = $GLOBALS['db_obj']->query($createsql);
	echo "2:创建新表完成。\n";
	
	echo "3:获取需要查取数据的库的新表。\n";
	//获取相应的库名
	$dabasesql = "select distinct(en_database_name) as en_database_name,diease_id from ". OLD_DB_TABLE ;
	$database_list = $GLOBALS['db_obj']->query($dabasesql);
//TODO =======================
//	$database_list = array();
//	$database_list[0]['en_database_name'] = 'lymphoma';
//	$database_list[0]['diease_id'] = '46';
//	$database_list[0]['en_database_name'] = 'lymph';
//	$database_list[0]['diease_id'] = '47';
//	$database_list[0]['en_database_name'] = 'lym-q';
//	$database_list[0]['diease_id'] = '118';
	if($database_list){
		for ($i = 0; $i < count($database_list); $i++) {
			echo "4." . $i . "查询表". $database_list[$i]['en_database_name'] ."数据开始。\n";
			//获取该库下面的所有医院及其科室，跟在新数据后面
			$pdo_config         = new MyPDOconfig(DB,DB_CONFIG);
    		$GLOBALS['db_obj']  =  $pdo_config->init_db();
    		$hospital_subject_sql = "select a.userid,a.group,b.group_id,b.hospital,b.subject from emr_member a,emr_member_group_org b where find_in_set('".$database_list[$i]['diease_id']."',b.dbs) and a.group_id=b.group_id";
    		$hospital_subject_list = $GLOBALS['db_obj']->query($hospital_subject_sql);
			$hospital_subject_array = array();
			for ($f = 0; $f < count($hospital_subject_list); $f++) {
    				$hospital_subject_array[$hospital_subject_list[$f]['userid']] = 	$hospital_subject_list[$f];
    		}
			//查询相应库中字段
			$pdo_config         = new MyPDOconfig(DB,DB_CONFIG);
    		$GLOBALS['db_obj']  =  $pdo_config->init_db();
    		$selectsql = "select * from ". OLD_DB_TABLE ." where en_database_name='" .$database_list[$i]['en_database_name']. "'";
    		$select_list = $GLOBALS['db_obj']->query($selectsql);
    		echo "4." . $i . "查询表". $database_list[$i]['en_database_name'] ."数据完毕1。\n";
    		if($select_list){
    			$db_select = "select id,userid";
    			$db_insert = "insert into ". NEW_DB_TABLE ." (data_from, hospital , h_subject ,import_id";
    			for ($j = 0; $j < count($select_list); $j++) {
						$db_select .=  " , " . $select_list[$j]['en_filed_name'] ;
						$db_insert .= " , " . $select_list[$j]['new_en_filed_name'] ;
    			}
    			$db_select .=  " from ". BASE_DB_TABLE."_". $database_list[$i]['en_database_name'] ;
    			$db_insert .=  ") values " ;
    			
    			//查询对应列的数据
    			$pdo_config         = new MyPDOconfig(DB,DB_CONFIG);
    			$GLOBALS['db_obj']  =  $pdo_config->init_db();
    			$selectresult = $GLOBALS['db_obj']->query($db_select);
    			echo "4." . $i . "查询表". $database_list[$i]['en_database_name'] ."数据完毕2。\n";
				//    			写入插入值
    			for($k = 0; $k < count($selectresult); $k++){
    				//添加值，按照相关顺序来的，data_from 插入值来自于哪个表
    				if($k==0){
    					$db_insert .= "('". $database_list[$i]['en_database_name'] ."'";
    				}else{
    					$db_insert .= ", ( '". $database_list[$i]['en_database_name'] ."'";
    				}
    				
    				if(empty($hospital_subject_array[$selectresult[$k]['userid']]['hospital'])){
    					$db_insert .= ", NULL " ;
    				}else{
    					$db_insert .= ",'" .$hospital_subject_array[$selectresult[$k]['userid']]['hospital'] ."'" ;
    				}
//    				$db_insert .= ",'" .$selectresult[$k]['hospital'] ."'" ;
    				
    				if(empty($hospital_subject_array[$selectresult[$k]['userid']]['subject'])){
    					$db_insert .= ", NULL " ;
    				}else{
    					$db_insert .= ",'" .$hospital_subject_array[$selectresult[$k]['userid']]['subject'] ."'" ;
    				}
    				$db_insert .= ",'" . $database_list[$i]['diease_id']."_". $selectresult[$k]['id'] ."'" ;
    				
    				for ($f = 0; $f < count($select_list); $f++) {
	    					if(!empty($selectresult[$k][$select_list[$f]['en_filed_name']])){
//	    						if($select_list[$f]['is_encrypt'] =='1'){
//	    							//需要解密字段，进行解密处理
//	    							file_put_contents("/tmp/transfer/ly.txt",addslashes(aesDecode($selectresult[$k][$select_list[$f]['en_filed_name']])) ."=111==",FILE_APPEND); 
//	    							$db_insert .= ", '" . addslashes(aesDecode($selectresult[$k][$select_list[$f]['en_filed_name']])) ."'";
//	    						}else{
	    							//此字段是否需要进行数据处理
	    							if(!empty($select_list[$f]['deal_method']) && $select_list[$f]['deal_method'] =='2'){
	    								$db_insert .= ", '" . addslashes(deal_method_2($selectresult[$k][$select_list[$f]['en_filed_name']])) ."'";
	    							}else if(!empty($select_list[$f]['deal_method']) && $select_list[$f]['deal_method'] =='3'){
	    								$db_insert .= ", '" . addslashes(deal_method_3($selectresult[$k][$select_list[$f]['en_filed_name']],json_decode($select_list[$f]['method3'], true))) ."'";
	    							}else if(!empty($select_list[$f]['deal_method']) && $select_list[$f]['deal_method'] =='4'){
	    								$db_insert .= ", '" . addslashes(deal_method_4($selectresult[$k][$select_list[$f]['en_filed_name']],json_decode($select_list[$f]['method3'], true))) ."'";
	    							}else if(!empty($select_list[$f]['deal_method']) && $select_list[$f]['deal_method'] =='5'){
	    								$db_insert .= ", '" . addslashes(deal_method_5($selectresult[$k][$select_list[$f]['en_filed_name']])) ."'";
	    							}else{
	    								$db_insert .= ", '" . addslashes($selectresult[$k][$select_list[$f]['en_filed_name']]) ."'";
	    							}
//	    						}
	    					}else{
	    						$db_insert .= ", NULL " ;
	    					}
	    			}
	    			$db_insert .= ")";
    			}
    			file_put_contents("/tmp/transfer/lym.txt",$db_insert,FILE_APPEND);
	   			//print $db_insert;die;
    			$pdo_config         = new MyPDOconfig(DB,DB_CONFIG);
    			$GLOBALS['db_obj']  =  $pdo_config->init_db();
    			$database_list[] = $GLOBALS['db_obj']->query($db_insert);
    			echo "5.插入数据完毕。\n";
    			//echo json_encode($database_list);die;
    		}
			
		}
	}
}

/**
 * 编号2（deal_method）
 * 文字转换成其他【999】
 */
function deal_method_2($str){
	if(!empty($str)){
		return '999';
	}else{
		return 'NULL';
	}
}


/**
 * 编号3（deal_method）
 * 规则:肺癌|1,肝癌|2,乳腺癌@宫颈癌@其他|999
 * 规则:1|9,2|8,3|999,4|999,5|999
 * 规则:1|5,2|6,3|7,4|8
 */
function deal_method_3($str,$deal_array){
	if(!empty($str) && !empty($deal_array)){
		if(isset($deal_array[$str])){
			return $deal_array[$str];
		}else{
			return $str; //正确匹配
		}
	}else{
		return '998'; //未知
	}
}


/**
 * 编号4（deal_method）
 * $str 数据库值（通过, 分割开来的）
 * $deal_array  （转化数组）
 *  合并字段，组织成多选
 * 
 */
function deal_method_4($str,$deal_array){
	$strtemp = '';
	if(!empty($str) && !empty($deal_array)){
		//多选转化成数组，单个单个的处理
		$str_array = $str.split(',');
		for($k = 0; $k < count($str_array); $k++){
			if($k + 1 == count($str_array)){//最后一个的时候最后不需要加,
				if(isset($deal_array[$str_array[$k]])){
					$strtemp.= $deal_array[$str_array[$k]];
				}else{
					$strtemp.=  $str_array[$k]; //无须匹配
				}
			}else{
				if(isset($deal_array[$str_array[$k]])){
					$strtemp.= $deal_array[$str_array[$k]] .",";
				}else{
					$strtemp.=  $str_array[$k] .","; //其他
				}
			}
		}
		return $strtemp;
	}else{
		return '998'; //未知
	}
}

/**
 * 时间转换（毫秒转化成时间格式y-m-d）
 */

function deal_method_5($str){
	if(!empty($str)){
		return date("Y-m-d H:i:s",time()) ;
	}else{
		return ''; //返回为空
	}
}







