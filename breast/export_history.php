<?php

/**

 * lym 库相关导出

 * create_time:2016-05-03

 * encoding : UTF-8

 */
ini_set("memory_limit","-1");
set_time_limit(0);

ini_set("display_errors","ON");

error_reporting(E_ALL);
//error_reporting(0);
define("BRD_WEB_ROOT", "/usr/local/var/www/test/");
require BRD_WEB_ROOT . "brdweb/base/config/base.inc.php";
require BRD_WEB_ROOT . "brdweb/lib/PHPExcel.php";
require BRD_WEB_ROOT . "brdweb/lib/PHPExcel/IOFactory.php";

date_default_timezone_set('Asia/Shanghai');

//需要update的数据库、表
define('DB','data-tranf');

define('OLD_TABLE_NAME','bs_history');
define('TABLE_NAME','bs_history_data');


//offline  测试库  line 线上库
define("DB_CONFIG", 'offline');

//结果输出文件
define('RESULT_PATH',"/tmp/update_data/".DB.".txt");

if (!file_exists(dirname(RESULT_PATH))) {

    mkdir(dirname(RESULT_PATH),0777,true);

}
file_put_contents(RESULT_PATH,'');


require '../script_pdo_config.php';

echo "\n**************************start***************\n";

//脚本运行开始

$start =  time();

echo "\nSCRIT RUN AT: ", date('Y-m-d H:i:s', time()), "\n";

main();

echo "\nSCRIT END AT: ", date('Y-m-d H:i:s', time()), "\n";

echo "TOTAL TIME : " . (time() - $start);

echo "\n++++++++++++++++++++++++++++++++++++++++++++OK++++++++++++++++++++++++++++++++++++++++++++++++++\n";

die;

/**
 * 数据导出
 */
function main(){
    $pdo_config         = new MyPDOconfig(DB,DB_CONFIG);

    $GLOBALS['db_obj']  =  $pdo_config->init_db();
    
    //查询出全部的医院名称
    $hospital_sql = "select DISTINCT(concat(IFNULL(data_from,'0'),\"-\",IFNULL(hospital,'0'),\"-\",IFNULL(h_subject,'0')))  as hospital from ". TABLE_NAME;
    $hospital_array = $GLOBALS['db_obj']->query($hospital_sql);
    
    //获取execl 列表的数组
    $colum_sql = "select new_en_filed_name,need_to_new_filed,is_encrypt from " .OLD_TABLE_NAME;
    $colum_array = $GLOBALS['db_obj']->query($colum_sql);
    $colum_temp = array();
   	//需要合并的字段集合
   	$merge_filed_array = array();
   	$$is_encrypt_array = array();
    for ($k = 0, $max_count = sizeof($colum_array); $k < $max_count; $k++) {
    	$is_encrypt = $colum_array[$k]['is_encrypt'];
		$new_en_filed_name = $colum_array[$k]['new_en_filed_name'];
		$need_to_new_filed = $colum_array[$k]['need_to_new_filed'];
		//计算竖列是否全部都为空
		 $count_colum_sql = "select count(id) as c_id from " .TABLE_NAME . " where " . $new_en_filed_name . " is not null";
   		 $count_colum = $GLOBALS['db_obj']->query($count_colum_sql);
		if(!empty($need_to_new_filed) && intVal($count_colum) > 0){
			$colum_temp[] = $need_to_new_filed;
			if(!in_array($new_en_filed_name,$merge_filed_array[$need_to_new_filed])){
				$merge_filed_array[$need_to_new_filed][] = $new_en_filed_name;
			}
			if(!empty($is_encrypt)){
    			$is_encrypt_array[$need_to_new_filed] = '1';
    		}	
		}else if(intVal($count_colum) > 0){
			$colum_temp[] = $new_en_filed_name;
			if(!empty($is_encrypt)){
    			$is_encrypt_array[$new_en_filed_name] = '1';
    		}
		}
	}
    $colum_temp = array_keys(array_flip($colum_temp));
	    if(!empty($hospital_array)){
	    	for ($index = 0, $max_count = sizeof($hospital_array); $index < $max_count; $index++) {
					$array_element = $hospital_array[$index]['hospital'];
					$export_sql = "select id,import_id";
						for ($i = 0; $i < sizeof($colum_temp); $i++) {
							if(!empty($merge_filed_array[$colum_temp[$i]])){
//								if($colum_temp[$i]=='id_no'){
//									$export_sql.=  ",IFNULL(IFNULL(id_no_1,id_no_2),id_no_3) as " .  $colum_temp[$i];
//								}else{
//									$export_sql.=  ",concat('['";
//									for ($h = 0; $h < sizeof($merge_filed_array[$colum_temp[$i]]); $h++) {
//										if($h==(sizeof($merge_filed_array[$colum_temp[$i]])-1)){
//											$export_sql.=  "," . $merge_filed_array[$colum_temp[$i]][$h] ;
//										}else{
//											$export_sql.=  "," . $merge_filed_array[$colum_temp[$i]][$h] .", ','";
//										}
//									}
//									$export_sql.= ",']') as " .  $colum_temp[$i];

									$export_sql.=  ",IFNULL(";
									for ($h = 0; $h < sizeof($merge_filed_array[$colum_temp[$i]]); $h++) {
										if($h== 0){
											$export_sql.= $merge_filed_array[$colum_temp[$i]][$h] ;
										}else{
											$export_sql.=  "," . $merge_filed_array[$colum_temp[$i]][$h] ;
										}
									}
									$export_sql.= ") as " .  $colum_temp[$i];

//								}
							}else{
								$export_sql.= "," . $colum_temp[$i];
							}
						}
						$export_sql.= "	 from ".TABLE_NAME." where concat(IFNULL(data_from,'0'), '-' ,IFNULL(hospital,'0'),'-',IFNULL(h_subject,'0')) = '".$array_element."' ";
						//	echo $export_sql;die;
							
							 $idarr = $GLOBALS['db_obj']->query($export_sql);
			
	        export_lym($colum_temp,$idarr,mb_substr($array_element,0,25,'utf-8'),$is_encrypt_array); 
	    	}
	    }
}

/**获取最大页数
 * @return float
 */

function get_table_sum($table_name)
{

    $sql = "SELECT count(*) as count from " . $table_name ;

    $result = $GLOBALS['db_obj']->query($sql);

    return $result[0]['count'];

}



// +----------------------------------------------------------------------
// | Sphynx PHPExcel导出Excel表格
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2014 http://www.sunnyos.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Sphynx <admin@sunnyos.com> QQ327388905
// +----------------------------------------------------------------------
/*
*+----------------------------------------------------------------------
*   PHPExcel导出Excel表格
*   array $rearr  需要导出的数组
*+----------------------------------------------------------------------
*/
function export_lym($colum_temp,$rearr,$array_element,$is_encrypt_array) {
	//echo json_encode($colum_temp);die;
    $arr = array(
        0 => 'A',
        1 => 'B',
        2 => 'C',
        3 => 'D',
        4 => 'E',
        5 => 'F',
        6 => 'G',
        7 => 'H',
        8 => 'I',
        9 => 'J',
        10 => 'K',
        11 => 'L',
        12 => 'M',
        13 => 'N',
        14 => 'O',
        15 => 'P',
        16 => 'Q',
        17 => 'R',
        18 => 'S',
        19 => 'T',
        20 => 'U',
        21 => 'V',
        22 => 'W',
        23 => 'X',
        24 => 'Y',
        25 => 'Z',
        26 => 'AA',
        27 => 'AB',
        28 => 'AC',
        29 => 'AD',
        30 => 'AE',
        31 => 'AF',
        32 => 'AG',
        33 => 'AH',
        34 => 'AI',
        35 => 'AJ',
        36 => 'AK',
        37 => 'AL',
        38 => 'AM',
        39 => 'AN',
        40 => 'AO',
        41 => 'AP',
        42 => 'AQ',
        43 => 'AR',
        44 => 'AS',
        45 => 'AT',
        46 => 'AU',
        47 => 'AV',
        48 => 'AW',
        49 => 'AX',
        50 => 'AY',
        51 => 'AZ'
    );
    // 创建一个excel
    $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;  
	$cacheSettings = array();  
	PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);
    $objPHPExcel = new PHPExcel();
    /****************************************设置居中开始**************************************/
    foreach ($arr as $key => $value) {
        $objPHPExcel->getActiveSheet()->getStyle($value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    }
    /****************************************设置居中结束**************************************/
    // 循环$arr定义的列设置每列内容居中
    $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")->setLastModifiedBy("Maarten Balliauw")->setTitle("Office 2007 XLSX Test Document")->setSubject("Office 2007 XLSX Test Document")->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")->setKeywords("office 2007 openxml php")->setCategory("Test result file");
    /**************************************设置标题开始*****************************************/
    // 循环$arr定义的列和$result设置表头
    $objPHPExcel->setActiveSheetIndex(0);
    for ($j = 0, $max_count = sizeof($colum_temp); $j < $max_count; $j++) {
    	//echo $j;echo $arr[$j];
		 $objPHPExcel->getActiveSheet()->setCellValue($arr[$j] . "1", $colum_temp[$j]);
	}
    
    /**************************************设置标题结束*****************************************/
    /**************************************设置内容开始*****************************************/
    $objPHPExcel->setActiveSheetIndex(0);
    $i = 2;
    // $rearr需要导出的数据二维数组
    foreach ($rearr as $svalue ) {
    	//echo json_encode($svalue['herb_name'])."\r\n";
        // 这里从二维数组里面通过键名获取到值放到相应的表格中
        for ($index = 0, $max_count = sizeof($colum_temp); $index < $max_count; $index++) {
        	if($colum_temp[$index] =='id_no'){
        		if(isset($is_encrypt_array[$colum_temp[$index]]) && !empty($svalue[$colum_temp[$index]])){
        			$objPHPExcel->getActiveSheet()->setCellValue($arr[$index] . $i, "'".aesDecode($svalue[$colum_temp[$index]]));
        		}else{
        			$objPHPExcel->getActiveSheet()->setCellValue($arr[$index] . $i, $svalue[$colum_temp[$index]]);
        		}
        	}else{
        		if(isset($is_encrypt_array[$colum_temp[$index]])){
        			$objPHPExcel->getActiveSheet()->setCellValue($arr[$index] . $i, aesDecode($svalue[$colum_temp[$index]]));
        		}else{
        			$objPHPExcel->getActiveSheet()->setCellValue($arr[$index] . $i, $svalue[$colum_temp[$index]]);
        		}
        	}
		}
        $i++;	
	}
    
    /**************************************设置内容结束*****************************************/
    /**************************************设置宽度开始*****************************************/
    // 循环$arr定义的列设置每列宽度
    foreach ($arr as $key => $value) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($value)->setWidth(20);
    }
    /**************************************设置宽度结束*****************************************/
    /**************************************设置导出下载开始*****************************************/
    $objPHPExcel->getSheet(0)->setTitle($array_element); // 工作区域标题
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$array_element.'_.xls"');//导出文件
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save(str_replace('.php', '_'.$array_element.'.xls', __FILE__));//导出文件
    /**************************************设置导出下载结束*****************************************/
}

?>






