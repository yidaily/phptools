<?php

/**

 * 淋巴瘤数据库数据迁移

 * create_time:2016-05-03

 * encoding : UTF-8

 */

set_time_limit(0);

ini_set("display_errors","ON");

error_reporting(E_ALL);
//error_reporting(0);
define("BRD_WEB_ROOT", "/usr/local/var/www/test/");
require BRD_WEB_ROOT . "brdweb/base/config/base.inc.php";

date_default_timezone_set('Asia/Shanghai');

//offline  测试库  line 线上库
define("DB_CONFIG", 'offline');

define('define_limit',1000);
//本地数据库库名
define('DB','data-tranf');
//数据字典数据库名
define('DATABASE','emr_disease');
//旧库的数据结构
define('OLD_DB_TABLE','bs_history');


//结果输出文件
define('RESULT_PATH',"/tmp/transfer/lym.txt");
if (!file_exists(dirname(RESULT_PATH))) {
    mkdir(dirname(RESULT_PATH),0777,true);
}
file_put_contents(RESULT_PATH,'');
define('RESULT_PATH1',"/tmp/transfer/ly.txt");
if (!file_exists(dirname(RESULT_PATH1))) {
    mkdir(dirname(RESULT_PATH1),0777,true);
}
file_put_contents(RESULT_PATH1,'');


require '../script_pdo_config.php';

echo "\n**************************start***************\n";

//脚本运行开始

$start =  time();

echo "\nSCRIT RUN AT: ", date('Y-m-d H:i:s', time()), "\n";
//1.把旧库的数据转化成新库的数据
//2.处理数据（数据合并，加密字段解密）
main();


echo "\nSCRIT END AT: ", date('Y-m-d H:i:s', time()), "\n";

echo "TOTAL TIME : " . (time() - $start);

echo "\n++++++++++++++++++++++++++++++++++++++++++++OK++++++++++++++++++++++++++++++++++++++++++++++++++\n";


die;


function main(){
   	//151 bc-a		174	bc-s	288	bc-l
  	$form_array = array('151' => array('form_id'=>'1039','en_database_name'=>'breast_v2','database_name'=>'乳腺癌-a'),'174'=>array('form_id'=>'1404','en_database_name'=>'bc_s','database_name'=>'乳腺癌-s'),'288'=>array('form_id'=>'2007','en_database_name'=>'bc_l','database_name'=>'乳腺癌-l'));
	
	echo "1.获取配置表所有数据\n";
	$pdo_config         = new MyPDOconfig(DB,DB_CONFIG);
    $GLOBALS['db_obj']  =  $pdo_config->init_db();
	//根据新字段创建新表
	$sql = "select * from ". OLD_DB_TABLE ." order by en_database_name";
	$data_list = $GLOBALS['db_obj']->query($sql);
	$filed_array = array();
	foreach ( $data_list as $key => $value ) {
       $filed_array[$value['diease_id']][$value['filed_name']] = $value;
	}
	
	echo "2.获取数据字典相关表所有数据\n";
	$pdo_config         = new MyPDOconfig(DATABASE,DB_CONFIG);
    $GLOBALS['db_obj']  =  $pdo_config->init_db();
    $disease_array = array();
	foreach ( $form_array as $key => $value ) {
       $sql = "select * from emr_disease_fields where disease_id = '".$key."' and form_id='".$value['form_id']."'";
       $disease_list = $GLOBALS['db_obj']->query($sql);
       for ($index = 0, $max_count = sizeof($disease_list); $index < $max_count; $index++) {
			$array_element = $disease_list[$index];
			$disease_array[$key][$array_element['field_name']] = $array_element;
		}
	}
	//file_put_contents("/tmp/transfer/ly.txt",json_encode($disease_array),FILE_APPEND); 
	echo "3.数据对比并且添加相应的数据。\n";
	$pdo_config         = new MyPDOconfig(DB,DB_CONFIG);
    $GLOBALS['db_obj']  =  $pdo_config->init_db();
    $insert_result = array();
	foreach ( $data_list as $key => $value ) {
    	  $temp_filed_name = $value['filed_name'];
    	  $temp_diease_id = $value['diease_id'];
    	  foreach ( $form_array as $k => $v ) {
       			if($k!=$temp_diease_id){
       				//判断其他库是否存在同名称字段，如果存在，就进行数据添加
       				if(isset($disease_array[$k][$temp_filed_name]) && !isset($filed_array[$k][$temp_filed_name])){
       					//存在，进行数据插入，拼接插入语句
       					$temp_sql = "insert into ".OLD_DB_TABLE." (diease_id,table_name,en_table_name,new_table_name,new_en_table_name,filed_name,en_filed_name,database_name,en_database_name,new_en_filed_name,new_filed_name,is_encrypt,need_to_new_filed,deal_method,method3)" .
       							"values ('".$k."','".$value['table_name']."','".$value['en_table_name']."','".$value['new_table_name']."','".$value['new_en_table_name']."','".$disease_array[$k][$temp_filed_name]['field_name']."','".$disease_array[$k][$temp_filed_name]['field_key'].
								"','".$v['database_name']."','".$v['en_database_name']."','".$value['new_en_filed_name']."','".$value['new_filed_name']."',". isnull($value['is_encrypt']) .",".isnull($value['need_to_new_filed']).",".isnull($value['deal_method']).",".isnull(addslashes($value['method3'])).")";
								
						//file_put_contents("/tmp/transfer/ly.txt",$temp_sql."\n",FILE_APPEND); 
						$insert_result[] = $GLOBALS['db_obj']->query($temp_sql);
       				}
       			}
		 }
	}
	echo json_encode($insert_result);
}


function isnull($str){
	if(!empty($str)){
		return "'" .$str ."'";
	}else{
		return "NULL"; //返回为空
	}
} 





